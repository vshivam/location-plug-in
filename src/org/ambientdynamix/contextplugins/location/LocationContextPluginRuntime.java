/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.AutoReactiveContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PluginAlert;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

public class LocationContextPluginRuntime extends AutoReactiveContextPluginRuntime implements LocationListener {
	private final String TAG = this.getClass().getSimpleName();
	private LocationManager _locMgr;
	private boolean _done;
	private int scanInterval;
	private HandlerThread ht = null;
	private Looper looper = null;
	private PowerScheme powerScheme;
	private boolean runOnce;
	private boolean registered;
	private List<UUID> locationRequests = new ArrayList<UUID>();
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	public void setPowerScheme(PowerScheme scheme) {
		Log.d(TAG, "Setting PowerScheme " + scheme);
		if (scheme == null) {
			Log.w(TAG, "Power Scheme was NULL");
			return;
		}
		powerScheme = scheme;
		runOnce = false;
		if (scheme.equals(PowerScheme.POWER_SAVER)) {
			scanInterval = 600000; // 10 minutes
			restartLocationEvents();
		} else if (scheme.equals(PowerScheme.BALANCED)) {
			scanInterval = 300000; // 5 minutes
			restartLocationEvents();
		} else if (scheme.equals(PowerScheme.HIGH_PERFORMANCE)) {
			scanInterval = 60000; // 1 minute
			restartLocationEvents();
		} else if (scheme.equals(PowerScheme.MANUAL)) {
			Log.w(TAG, "Manual power scheme not supported yet...");
		} else
			Log.w(TAG, "Unknown PowerScheme: " + scheme);
	}

	public void destroy() {
		stop();
		_locMgr = null;
		Log.d(TAG, "Destroyed!");
	}

	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		this.powerScheme = scheme;
		_locMgr = (LocationManager) getSecuredContext().getSystemService(Context.LOCATION_SERVICE);
		if (_locMgr == null) {
			throw new Exception("Could not access Location Service - Check Permissions");
		}
	}

	@Override
	public void doManualContextScan() {
		// TODO Auto-generated method stub
	}

	@Override
	public void start() {
		if (!_locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			Log.w(TAG, "GPS hardware disabled");
			try {
				// Alert that GPS is not enabled
				PluginAlert alert = new PluginAlert("GPS Warning",
						"GPS hardware is disabled. Enable GPS hardware to resume location tracking.",
						Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				getPluginFacade().sendPluginAlert(getSessionId(), alert);
			} catch (Exception e) {
				Log.w(TAG, "Could not send PluginAlert!");
			}
		}
		deregisterLocationRequest();
		_done = false;
		ht = new HandlerThread(TAG);
		ht.start();
		looper = ht.getLooper();
		registerLocationRequest();
		Log.d(TAG, "startContextScanning!");
		// loop until we're stopped
		while (!_done) {
			// See: http://marakana.com/forums/android/android_examples/42.html
			// Wake our thread up periodically to check for done
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				_done = true;
			}
		}
		if (looper != null)
			looper.quit();
		ht = null;
		Log.d(TAG, "startContextScanning exiting!");
	}

	public void stop() {
		deregisterLocationRequest();
		_done = true;
		// Cancel outstanding requests
		synchronized (locationRequests) {
			for (UUID req : locationRequests) {
				getPluginFacade().cancelContextRequestId(getSessionId(), req);
			}
		}
	}

	public void onLocationChanged(Location loc) {
		if (loc != null) {
			// Send context event
			sendBroadcastContextEvent(new SecuredContextInfo(new LocationContextInfo(loc), PrivacyRiskLevel.MAX),
					scanInterval);
			synchronized (locationRequests) {
				for (UUID req : locationRequests) {
					sendContextEvent(req, new SecuredContextInfo(new LocationContextInfo(loc), PrivacyRiskLevel.MAX),
							scanInterval);
				}
			}
		}
		if (runOnce)
			stop();
	}

	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
	}

	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// TODO Auto-generated method stub
	}

	private void restartLocationEvents() {
		deregisterLocationRequest();
		registerLocationRequest();
	}

	private synchronized void registerLocationRequest() {
		/*
		 * Background services should be careful about setting a sufficiently high minTime so that the device doesn't
		 * consume too much power by keeping the GPS or wireless radios on all the time. In particular, values under
		 * 60000ms are not recommended.
		 */
		if (!registered) {
			Log.d(TAG, "Scanning for GPS data...");
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_COARSE); // Faster, no GPS fix.
			criteria.setAccuracy(Criteria.ACCURACY_FINE); // More accurate, GPS fix.
			_locMgr.requestLocationUpdates(_locMgr.getBestProvider(criteria, true), scanInterval,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this, looper);
			registered = true;
			// Try to provide an initial location event based on our last known location
			Location l = getLastKnownLocation();
			if (l != null)
				sendBroadcastContextEvent(new SecuredContextInfo(new LocationContextInfo(l), PrivacyRiskLevel.MAX),
						scanInterval);
		}
	}

	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle scanConfig) {
		Log.w(TAG, "Scan configuration not supported by: " + this);
		handleContextRequest(requestId, contextInfoType);
	}

	@Override
	public void handleContextRequest(UUID requestId, String contextInfoType) {
		if (contextInfoType.equalsIgnoreCase("org.ambientdynamix.contextplugins.location")) {
			Location l = getLastKnownLocation();
			if (l != null)
				sendContextEvent(requestId, new SecuredContextInfo(new LocationContextInfo(l), PrivacyRiskLevel.MAX),
						scanInterval);
			else {
				synchronized (locationRequests) {
					locationRequests.add(requestId);
				}
				registerLocationRequest();
				// sendContextRequestError(requestId, "Could not find last known location",
				// ErrorCodes.INTERNAL_PLUG_IN_ERROR)
			}
		} else {
			Log.w(TAG, "Context type not supported: " + contextInfoType);
			sendContextRequestError(requestId, new String(contextInfoType + " not supported"),
					ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
		}
	}

	private Location getLastKnownLocation() {
		Location l = _locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (l != null)
			return l;
		else
			l = _locMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		return l;
	}

	private synchronized void deregisterLocationRequest() {
		if (registered) {
			_locMgr.removeUpdates(this);
			registered = false;
		}
	}
}