/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.location;

import java.util.HashSet;
import java.util.Set;

import org.ambientdynamix.api.application.IContextInfo;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

class LocationContextInfo implements IContextInfo, ILocationContextInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static final Parcelable.Creator<LocationContextInfo> CREATOR = new Parcelable.Creator<LocationContextInfo>() {
		public LocationContextInfo createFromParcel(Parcel in) {
			return new LocationContextInfo(in);
		}

		public LocationContextInfo[] newArray(int size) {
			return new LocationContextInfo[size];
		}
	};
	// Private data
	private double latitude;
	private double longitude;
	private double altitude;
	private float speed;
	private float bearing;
	private float accuracy;
	private long time;
	private String provider;

	public LocationContextInfo(Location loc) {
		this.latitude = loc.getLatitude();
		this.longitude = loc.getLongitude();
		this.altitude = loc.getAltitude();
		this.speed = loc.getSpeed();
		this.bearing = loc.getBearing();
		this.time = loc.getTime();
		this.accuracy = loc.getAccuracy();
		this.provider = loc.getProvider();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain")) {
			return this.latitude + ":" + this.longitude;
		} else
			return "";
	}
	/**
	 * {@inheritDoc}
	 */
	public double getLatitude() {
		return latitude;
	}
	/**
	 * {@inheritDoc}
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * {@inheritDoc}
	 */
	public double getAltitude() {
		return altitude;
	}
	/**
	 * {@inheritDoc}
	 */
	public float getSpeed() {
		return speed;
	}
	/**
	 * {@inheritDoc}
	 */
	public float getBearing() {
		return bearing;
	}
	/**
	 * {@inheritDoc}
	 */
	public float getAccuracy() {
		return accuracy;
	}
	/**
	 * {@inheritDoc}
	 */
	public long getTime() {
		return time;
	}
	/**
	 * {@inheritDoc}
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.location";
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	public IBinder asBinder() {
		return null;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int arg1) {
		out.writeDouble(latitude);
		out.writeDouble(longitude);
		out.writeDouble(altitude);
		out.writeFloat(speed);
		out.writeFloat(bearing);
		out.writeLong(time);
		out.writeFloat(accuracy);
		out.writeString(provider);
	}

	private LocationContextInfo(final Parcel in) {
		latitude = in.readDouble();
		longitude = in.readDouble();
		altitude = in.readDouble();
		speed = in.readFloat();
		bearing = in.readFloat();
		time = in.readLong();
		accuracy = in.readFloat();
		provider = in.readString();
	}
}
